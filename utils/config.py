import os

from cnn.models.ef import EF
from cnn.models.ef_3 import EF_3
from cnn.models.mt import MT
from cnn.models.mt_3 import MT_3
from cnn.models.mt_resnet import MT_ResNet
from cnn.models.mt_3_resnet import MT_3_ResNet
from cnn import layers
import datetime

config = {}

### CHOSE THE DATASET HERE ###
datasets = {'lrw':'lrw', 'lrs':'lrs', 'mnist':'mnist'}
config['dataset'] = datasets['lrw']
config['dataset_types'] = ['train', 'val', 'test']

# CHANGE THIS IF YOU WANT ONLY TO START TESTING THE MODEL!!!
config['is_testing'] = False

# CHANGE THIS IF YOU WANT SKIP CONVERTING VIDEOS TO TFRECORS
config['should_write_lrw_to_tfrecords'] = False

config['mnist_w'] = 28
config['mnist_h'] = 28
config['mnist_c'] = 1
config['mnist_classes'] = 10

config['input_w'] = 112 if config['dataset'] != 'mnist' else config['mnist_w']
config['input_h'] = 112 if config['dataset'] != 'mnist' else config['mnist_h']
config['input_c'] = 3 if config['dataset'] != 'mnist' else config['mnist_c']
config['num_classes'] = 500 if config['dataset'] != 'mnist' else config['mnist_classes']
config['frames'] = 25

config['write_batch_size'] = 2000
config['write_w'] = config['input_w']
config['write_h'] = config['input_h']
config['write_c'] = config['input_c']

config['max_epochs'] = 10
config['batch_size'] = 20
config['log_every'] = 1000
config['save_every'] = 0.05 if config['dataset'] != 'mnist' else 0.25
config['learning_rate'] = 5e-4
config['decay_steps'] = 100000
config['decay_rate'] = 0.96
config['pool_size'] = [3, 3]
config['stride'] = 2
config['weight_mean'] = 0
config['weight_variance'] = 10e-2

initializers = {'xavier' : layers.xavier_initializer()}
config['initializer'] = initializers['xavier']

config['regularizer_scale'] = 3.4
regularizers = { 'l1' : layers.l1_regularizer(config['regularizer_scale']), 'l2' : layers.l2_regularizer(config['regularizer_scale'])}
config['regularizer'] = regularizers['l2']

config['use_se'] = False

activations = {'relu':layers.relu, 'selu':layers.selu, 'sigmoid':layers.sigmoid}
config['activation_fn'] = activations['relu']

### CHOOSE THE MODEL HERE ###
models = {'ef': EF(), 'ef_3':EF_3(), 'mt':MT(), 'mt_3':MT_3(), 'mt_resnet' : MT_ResNet(), 'mt_3_resnet' : MT_3_ResNet()}
chosen_model_name = 'ef'
config['model'] = models[chosen_model_name]
config['model_name'] = config['model'].name
config['use_resnet'] = True if 'resnet' in chosen_model_name else False

optimizators = {'sgd' : layers.sgd, 'adam' : layers.adam, 'adagrad' : layers.adagrad, 'adagradda' : layers.adagradDA, 'adadelta' : layers.adadelta}
config['optimizer'] = optimizators['adam']

# APPLICATION ROOT DIR
config['root_path'] = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# DATA ROOT DIR - FOR ALL DATA, RESULTS, SUMMARIES...
config['data_root_dir'] = os.path.join(config['root_path'], 'data')

# DATASET ROOT DIR - DIR WITH MP4 VIDEOS - PER DATASET
custom_dataset_root = 'D:/faks/diplomski/lipreading-data/data/lrw'
default_dataset_root = os.path.join(config['data_root_dir'], 'datasets', config['dataset'])
config['dataset_root_dir'] = custom_dataset_root if config['dataset'] != 'mnist' else default_dataset_root

# DIR FOR SAVING PDFS AND CSV RESULTS - PER DATASET AND MODEL
config['results_root_dir'] = os.path.join(config['data_root_dir'], 'results', config['dataset'], config['model_name'])

# DIR FOR SAVING SESSION VARIABLES - PER DATASET AND MODEL
config['saved_session_root_dir'] = os.path.join(config['data_root_dir'], 'saved_session', config['dataset'], config['model_name'])

# DIR FOR SAVING SUMMARIES - PER DATASET AND MODEL
config['summary_root_dir'] = os.path.join(config['data_root_dir'], 'summary', config['dataset'], config['model_name'])

# DIR FOR TFRECORDS BINARIES - PER DATASET
config['tfrecords_root_dir'] = os.path.join(config['data_root_dir'], 'tfrecords', config['dataset'] + '_tfrecords')

# MAP DIRS FOR TFRECORDS - PER TRAIN, VALID, TEST
config['tfrecords_data'] = {'train' : os.path.join(config['tfrecords_root_dir'], 'train'),
                  'val': os.path.join(config['tfrecords_root_dir'], 'val'),
                  'test': os.path.join(config['tfrecords_root_dir'], 'test')}

# MAP DIRS FOR SUMMARIES - SUMMARY FOR TRAINING, FOR VALIDATING TRAINSET, VALIDATING VALIDSET, VALIDATING TESTSET
config['summary_data'] = {'train_train' : os.path.join(config['summary_root_dir'], 'train_train'),
                  'valid_train': os.path.join(config['summary_root_dir'], 'valid_train'),
                  'valid_valid': os.path.join(config['summary_root_dir'], 'valid_valid'),
                  'test': os.path.join(config['summary_root_dir'], 'test')}

# DIR FOR LOGGING
config['log_dir'] = os.path.join(config['data_root_dir'], 'log', config['dataset'], config['model_name'])

# FILE FOR MAPPING LABELS TO NUMBERS
config['classmapfile'] = os.path.join(config['tfrecords_root_dir'], config['dataset'] + '_classmap.txt')

# FILE PATTERN
config['results_filename'] = datetime.datetime.now().strftime('%Y_%m_%d_%H_%M')

# EXTENSIONS
PDF_EXT = '.pdf'
CSV_EXT = '.csv'
TXT_EXT = '.txt'

# ALL DIRS THAT ARE IMPORTANT FOR APPLICATION (like storing data, reading data, dirs for summaries, results...)
DIRS_TO_CREATE = [config['data_root_dir'], config['dataset_root_dir'], config['results_root_dir'], config['saved_session_root_dir'], config['summary_root_dir'],
           config['tfrecords_root_dir'], config['tfrecords_data']['train'], config['tfrecords_data']['val'], config['tfrecords_data']['test'],
           config['summary_data']['train_train'], config['summary_data']['valid_train'], config['summary_data']['valid_valid'], config['summary_data']['test'],
           config['log_dir']]

