from utils import config, util
from read_write.tf_records_reader import TFRecordsReader
import tensorflow as tf
from read_write.read_mnist import ReadMnist
import numpy as np
import random

class Dataset():

    def __init__(self, tfSession):

        self.tfSession = tfSession
        self.dataset_name = config.config['dataset']
        self.datasetRootDir = config.config['dataset_root_dir']
        self.max_epochs = config.config['max_epochs']
        self.batch_size = config.config['batch_size']
        self.num_train_examples = 0
        self.num_val_examples = 0
        self.num_test_examples = 0
        self.frames = config.config['frames']
        self.num_classes = config.config['num_classes']
        self.mapperDatatype = {'train_train' : 'train', 'valid_train' : 'train', 'valid_valid' : 'val', 'test' : 'test'}

        if self.dataset_name == 'mnist':
            self.mnist = ReadMnist()
            self.num_train_examples = self.mnist.num_train
            self.num_val_examples = self.mnist.num_val
            self.num_test_examples = self.mnist.num_test
        else:
            with tf.device('/CPU:0'):

                self.tfTrainTrainReader = TFRecordsReader()
                self.tfValidTrainReader = TFRecordsReader()
                self.tfValidValidReader = TFRecordsReader()
                self.tfTestReader = TFRecordsReader()

                self.tfTrainTrainReader.create_iterator('train', self.max_epochs, self.batch_size)
                self.tfValidTrainReader.create_iterator('train', self.max_epochs, self.batch_size)
                self.tfValidValidReader.create_iterator('val', self.max_epochs, self.batch_size)
                self.tfTestReader.create_iterator('test', self.max_epochs, self.batch_size)

                self.num_train_examples = util.numberOfData('train')
                self.num_val_examples = util.numberOfData('val')
                self.num_test_examples = util.numberOfData('test')

                self.readers = {'train_train' : self.tfTrainTrainReader, 'valid_train' : self.tfValidTrainReader,
                                'valid_valid' : self.tfValidValidReader, 'test' : self.tfTestReader}

        self.num_batches_train = self.num_train_examples // self.batch_size
        self.num_batches_val = self.num_val_examples // self.batch_size
        self.num_batches_test = self.num_test_examples // self.batch_size

    def getBatch(self, datasetType):

        if self.dataset_name == 'mnist':
            return self.mnist.get_batch(self.mapperDatatype[datasetType])
        else:
            batch_x, batch_y = self.tfSession.run([self.readers[datasetType].images, self.readers[datasetType].labels])
            batch_x = np.array(batch_x)[:, sorted(random.sample(range(0, 29), self.frames))]
            batch_y = util.class_to_onehot(batch_y, self.num_classes)
            return batch_x, batch_y
