import os
import tensorflow as tf
import numpy as np
from random import shuffle
from read_write.lrw_reader import LRWReader
from utils import config
from utils import util
from imaging.image_resize import ImageResizer

class TFRecordsWriter():

    def start_lrw_writer(self):

        videos_dir = config.config['dataset_root_dir']
        dataset_types = config.config['dataset_types']
        classmapfile = config.config['classmapfile']

        if util.isDirOrFileExist(classmapfile):
            lrwClasses = util.readClassmapFile(classmapfile, numsAsKeys=False)
        else:
            lrwClasses = util.lrwWordsToNumbers(videos_dir)
            util.writeClassmapFile(classmapfile, lrwClasses)

        lrwReader = LRWReader()

        for datasetType in dataset_types:
            lrwMap = lrwReader.readLRWtoMap(videos_dir, datasetType)
            self.write_records(lrwMap, lrwClasses, 'lrw', datasetType)

    def write_records(self, videoMap, videoClassMap, datasetname, dataset_type):

        print("Creating dir for saving tf records!")
        dirpath = util.create_dir(config.config['tfrecords_data'][dataset_type])

        print("Clearing already written items!")
        videoMap = self.clear_already_written(dirpath, videoMap)

        print("Shuffling data!")
        videoList = list(videoMap.items())
        shuffle(videoList)

        print("Starting processing {} images".format(dataset_type))
        batchSize = config.config['write_batch_size']
        batchIndexer = self.get_number_of_written_tfrecords(dirpath)
        totalBatches = int(len(videoList)/batchSize) + 1
        batchCounter = 0

        while len(videoList) > 0:

            print("Writing batch {}/{}".format(batchCounter + 1, totalBatches))

            currentBatch = videoList[:batchSize]
            if len(videoList) <= batchSize:
                videoList = []
            else:
                videoList = videoList[batchSize:]

            batchWithImagesList = []

            for video in currentBatch:
                resizedVideos = len(batchWithImagesList)
                if not resizedVideos % 100:
                    print("Videos read and resized: {}/{}".format(resizedVideos, len(currentBatch)))
                images = util.video_to_images(video[0])
                images = self.resize_images(images)
                batchWithImagesList.append((video[0], video[1], videoClassMap[video[1]], images))

            batch_filename = os.path.join(dirpath, '{}_{}_batch_{}.{}'.format(datasetname, dataset_type, batchIndexer, "tfrecords"))
            self.write(batch_filename, batchWithImagesList, dataset_type)

            batch_stat_filename = os.path.join(dirpath, '{}_{}_batch_{}.{}'.format(datasetname, dataset_type, batchIndexer, "txt"))
            self.write_written_images(batch_stat_filename, batchWithImagesList)

            batchIndexer += 1
            batchCounter += 1
			
            print('BATCH FINISHED')
            exit(1);

    def write(self, filename, batchWithImagesList, dataset_type):

        print("\nWriting batch -> {}".format(filename))

        writer = tf.python_io.TFRecordWriter(filename)

        videoCounter = 0

        for item in batchWithImagesList:

            if not videoCounter % 200:
                print("Videos: {}/{}".format(videoCounter, len(batchWithImagesList)))

            images = np.ascontiguousarray(item[3])
            label = np.ascontiguousarray(item[2])

            feature = {dataset_type + '/label': util._int64_feature(np.asscalar(label)),
                       '{}/video'.format(dataset_type): util._bytes_feature(tf.compat.as_bytes(images.tostring()))}

            example = tf.train.Example(features=tf.train.Features(feature=feature))

            writer.write(example.SerializeToString())

            videoCounter += 1

        writer.close()

    def write_written_images(self, batchStatFilename, videoList):
        file = open(batchStatFilename, 'w', encoding='utf-8')
        for item in videoList:
            file.write('{};{};{}\n'.format(item[0], item[1], item[2]))

        file.close()

    def clear_already_written(self, tfrecordsDir, videoMapToClear):

        for file in os.listdir(tfrecordsDir):
            filename = os.fsdecode(file)
            current_batch_tfrecord_stat = os.path.join(tfrecordsDir, filename)
            if current_batch_tfrecord_stat.endswith(".txt"):
                file = open(current_batch_tfrecord_stat, 'r', encoding='utf-8')
                for video in file.readlines():
                    videoMapToClear.pop(video.split(';')[0].replace('\n', ''), None)

        return videoMapToClear

    def get_number_of_written_tfrecords(self, tfrecordsDir):
        return int(round(len(os.listdir(tfrecordsDir)) / 2))

    def resize_images(self, images):
        imgResizer = ImageResizer()
        return imgResizer.start_resizing(images, 112, 112)

