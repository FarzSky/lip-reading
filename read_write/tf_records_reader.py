import os
import tensorflow as tf
import numpy as np

from utils import config

class TFRecordsReader():

    def __init__(self):

        self.fileDict = config.config['tfrecords_data']

        self.input_w = config.config['input_w']
        self.input_h = config.config['input_h']
        self.input_c = config.config['input_c']

    def getTfRecordFiles(self, datasettype):

        dataDir = self.fileDict[datasettype]

        tfRecordsList = []

        if not os.path.exists(dataDir):
            return tfRecordsList

        for file in os.listdir(dataDir):
            filename = os.fsdecode(file)
            if filename.endswith(".tfrecords"):
                tfRecordsList.append(os.path.join(dataDir, filename))

        return tfRecordsList

    def create_iterator(self, dataset_type, num_epochs, batch_size):

        with tf.variable_scope('tf_record_reader'):

            feature = {dataset_type + '/video': tf.FixedLenFeature([], tf.string),
                            dataset_type + '/label': tf.FixedLenFeature([], tf.int64)}

            filename_queue = tf.train.string_input_producer(self.getTfRecordFiles(dataset_type), shuffle=True, num_epochs=num_epochs)

            reader = tf.TFRecordReader()

            self.images = []
            self.labels = []

            for i in range(batch_size):

                _, ser_example = reader.read(filename_queue)

                features = tf.parse_single_example(ser_example, features=feature)

                image = tf.decode_raw(features[dataset_type + '/video'], tf.uint8)
                label = tf.cast(features[dataset_type + '/label'], tf.int32)

                self.images.append(tf.reshape(image, [29, self.input_w, self.input_h, self.input_c]))
                self.labels.append(label)