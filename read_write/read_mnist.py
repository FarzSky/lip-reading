from tensorflow.examples.tutorials.mnist import input_data
from utils import config

class ReadMnist():

    def __init__(self):
        self.name = 'mnist'
        self.mnist_dir = config.config['dataset_root_dir']
        self.mnist_w = config.config['input_w']
        self.mnist_h = config.config['input_h']
        self.mnist_c = config.config['input_c']
        self.batch_size = config.config['batch_size']
        self.num_train = 0
        self.num_val = 0
        self.num_test = 0
        self.train_index = 0
        self.val_index = 0
        self.test_index = 0
        self.read_mnist()

    def read_mnist(self):

        dataset = input_data.read_data_sets(self.mnist_dir, one_hot=True)

        train_x = dataset.train.images
        self.train_x = train_x.reshape([-1, self.mnist_w, self.mnist_h, self.mnist_c])
        self.train_y = dataset.train.labels

        valid_x = dataset.validation.images
        self.valid_x = valid_x.reshape([-1, self.mnist_w, self.mnist_h, self.mnist_c])
        self.valid_y = dataset.validation.labels

        test_x = dataset.test.images
        self.test_x = test_x.reshape([-1,self.mnist_w, self.mnist_h, self.mnist_c])
        self.test_y = dataset.test.labels

        self.num_train = dataset.train.num_examples
        self.num_val = dataset.validation.num_examples
        self.num_test = dataset.test.num_examples

    def get_batch(self, datasettype='train'):

        if datasettype == 'val':
            return self.get_valid_batch()
        elif datasettype == 'test':
            return self.get_test_batch()
        else:
            return self.get_train_batch()

    def get_train_batch(self):
        batch_x = self.train_x[self.train_index:self.train_index + self.batch_size]
        batch_y = self.train_y[self.train_index:self.train_index + self.batch_size]
        self.train_index += self.batch_size
        if self.train_index >= self.num_train:
            self.train_index = 0
        return (batch_x, batch_y)

    def get_valid_batch(self):
        batch_x = self.valid_x[self.val_index:self.val_index + self.batch_size]
        batch_y = self.valid_y[self.val_index:self.val_index + self.batch_size]
        self.val_index += self.batch_size
        if self.val_index >= self.num_val:
            self.val_index = 0
        return (batch_x, batch_y)

    def get_test_batch(self):
        batch_x = self.test_x[self.test_index:self.test_index + self.batch_size]
        batch_y = self.test_y[self.test_index:self.test_index + self.batch_size]
        self.test_index += self.batch_size
        if self.test_index >= self.num_test:
            self.test_index = 0
        return (batch_x, batch_y)