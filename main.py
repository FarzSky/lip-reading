from cnn.cnn import CNN
from read_write.tf_records_writer import TFRecordsWriter
import tensorflow as tf
from utils import config

def main():

    is_testing = config.config['is_testing']
    should_write_lrw_to_tfrecords = config.config['should_write_lrw_to_tfrecords']

    writeLRWToTFRecords(should_write_lrw_to_tfrecords)
    startTrain(is_testing)

def startTrain(is_testing=False):

    print("STARED TRAINING PROCESS!")
    trainer = CNN(is_testing=is_testing)

    if is_testing:
        trainer.test()
    else:
        trainer.train()

def writeLRWToTFRecords(should_write_lrw_to_tfrecords=True):

    if should_write_lrw_to_tfrecords:
        with tf.device("/GPU:0"):
            tfRecordsWriter = TFRecordsWriter()
            tfRecordsWriter.start_lrw_writer()

main()
