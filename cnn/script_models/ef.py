from utils import config, util
from cnn import layers
import tensorflow as tf
import tensorflow.contrib.layers as tflayers
import time
from data.dataset import Dataset
import os
import numpy as np

class EF:

    def __init__(self):

        self.initConfig()
        self.createPlotDataVars()
        with tf.device("/GPU:0"):
            self.createModel()
        self.createSession()
        self.initDataReaders()
        self.initSummaries()
        self.initSessionVariables()
        self.createRestorer()

    def createModel(self):

        print("CREATING MODEL")

        self.initializer = tflayers.xavier_initializer()
        self.regularizer = tflayers.l2_regularizer(self.regularizer_scale)

        shape = [None, self.frames, self.h, self.w, self.c]
        if self.dataset_name == 'mnist':
            shape = [None, self.h, self.w, self.c]
        self.X = tf.placeholder(name='image', dtype=tf.float32, shape=shape)
        self.Yoh = tf.placeholder(name='label', dtype=tf.int64, shape=[None, self.num_classes])

        self.global_step = tf.Variable(0, trainable=False)
        self.is_training = tf.placeholder_with_default(True, [], name='is_training')

        self.activation_fn = layers.relu

        net = self.X

        if self.dataset_name != 'mnist':
            net = layers.rgb_to_grayscale(net)
            net = tf.transpose(net, [0, 2, 3, 1, 4])
            net = tf.reshape(net, [-1, net.shape[1], net.shape[2], net.shape[3] * net.shape[4]])

        net = tflayers.conv2d(net, num_outputs=96, kernel_size=[3, 3], padding='valid', stride=2, scope='conv1',
                           activation_fn=self.activation_fn, weights_initializer=self.initializer, weights_regularizer=self.regularizer)
        net = tflayers.batch_norm(inputs=net, is_training=self.is_training, scope='bn1')
        net = tf.nn.relu(net)
        net = tflayers.max_pool2d(net, [3, 3], 2, padding='valid', scope='max_pool1')

        net = tflayers.conv2d(net, num_outputs=256, kernel_size=[3, 3], padding='valid', stride=2, scope='conv2',
                           activation_fn=self.activation_fn, weights_initializer=self.initializer, weights_regularizer=self.regularizer)
        net = tflayers.batch_norm(inputs=net, is_training=self.is_training, scope='bn2')
        net = tf.nn.relu(net)
        net = tflayers.max_pool2d(net, [3, 3], 2, padding='valid', scope='max_pool2')

        net = tflayers.conv2d(net, num_outputs=512, kernel_size=[3, 3], padding='same', stride=1, scope='conv3',
                           activation_fn=self.activation_fn, weights_initializer=self.initializer, weights_regularizer=self.regularizer)

        net = tflayers.conv2d(net, num_outputs=512, kernel_size=[3, 3], padding='same', stride=1, scope='conv4',
                           activation_fn=self.activation_fn, weights_initializer=self.initializer, weights_regularizer=self.regularizer)

        net = tflayers.conv2d(net, num_outputs=512, kernel_size=[3, 3], padding='same', stride=1, scope='conv5',
                           activation_fn=self.activation_fn, weights_initializer=self.initializer, weights_regularizer=self.regularizer)
        net = tflayers.max_pool2d(net, [3, 3], 2, padding='valid', scope='max_pool5')

        net = tflayers.flatten(net, scope='flatten')

        net = tflayers.fully_connected(net, 4096, self.activation_fn, scope='fc6', weights_initializer=self.initializer, weights_regularizer=self.regularizer)
        net = tflayers.fully_connected(net, 4096, self.activation_fn, scope='fc7', weights_initializer=self.initializer, weights_regularizer=self.regularizer)
        net = tflayers.fully_connected(net, self.num_classes, scope='fc8', weights_initializer=self.initializer, weights_regularizer=self.regularizer)

        self.logits = net
        self.preds = tf.nn.softmax(self.logits)
        self.loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits=self.logits, labels=self.Yoh))
        self.learning_rate = tf.train.exponential_decay(self.learning_rate, self.global_step, self.decay_steps, self.decay_rate, staircase=False, name='learning_rate_decay')
        self.accuracy, self.precision, self.recall, self.precAtTopK, self.recAtTopK = self.createSummaries(self.Yoh, self.preds, self.loss, self.learning_rate)
        self.opt = tf.train.AdamOptimizer(self.learning_rate)
        self.train_op = self.opt.minimize(self.loss, global_step=self.global_step)

    def train(self):

        print("TRAINING IS STARTING RIGHT NOW!")

        for epoch_num in range(1, self.max_epochs + 1):
            epoch_start_time = time.time()
            currentSaveRate = self.save_every
            for step in range(self.dataset.num_batches_train):
                start_time = time.time()

                batch_x, batch_y = self.dataset.getBatch('train_train')
                feed_dict = {self.X: batch_x, self.Yoh: batch_y, self.is_training: True}
                eval_tensors = [self.train_op, self.loss]
                if (step + 1) * self.batch_size % self.log_every == 0:
                    eval_tensors += [self.merged_summary_op, self.accuracy, self.precision, self.recall, self.precAtTopK, self.recAtTopK]

                eval_ret = self.sess.run(eval_tensors, feed_dict=feed_dict)

                eval_ret = dict(zip(eval_tensors, eval_ret))
                loss_val = eval_ret[self.loss]
                if self.merged_summary_op in eval_tensors:
                    self.summary_train_train_writer.add_summary(eval_ret[self.merged_summary_op], self.global_step.eval(session=self.sess))

                duration = time.time() - start_time
                util.log_step(epoch_num, step, duration, loss_val, self.batch_size, self.dataset.num_train_examples, self.log_every)

                if (step / self.dataset.num_batches_train) >= (currentSaveRate):
                    self.saver.save(self.sess, os.path.join(self.saved_session_dir, config.config['model_name'] + '.ckpt'))
                    currentSaveRate += self.save_every

            epoch_time = time.time() - epoch_start_time
            print("Total epoch time training: {}".format(epoch_time))

            self.startValidation(epoch_num, epoch_time)
            self.startTesting()

        self.finishTraining()

    def startValidation(self, epoch_num, epoch_time):

        print("EPOCH VALIDATION : ")

        train_loss, train_acc, train_pr, train_rec = self.validate(self.dataset.num_train_examples, epoch_num, "valid_train")
        valid_loss, valid_acc, valid_pr, valid_rec = self.validate(self.dataset.num_val_examples, epoch_num, "valid_valid")
        lr = self.sess.run([self.learning_rate])

        self.plot_data['epoch_time'] += [epoch_time]
        self.plot_data['train_loss'] += [train_loss]
        self.plot_data['train_acc'] += [train_acc]
        self.plot_data['train_pr'] += [train_pr]
        self.plot_data['train_rec'] += [train_rec]
        self.plot_data['valid_loss'] += [valid_loss]
        self.plot_data['valid_acc'] += [valid_acc]
        self.plot_data['valid_pr'] += [valid_pr]
        self.plot_data['valid_rec'] += [valid_rec]
        self.plot_data['lr'] += [lr]

        util.plot_training_progress(self.plot_data)

    def validate(self, num_examples, epoch, dataset_type="Unknown"):

        losses = []
        eval_preds = np.ndarray((0,), dtype=np.int64)
        labels = np.ndarray((0,), dtype=np.int64)
        num_batches = num_examples // self.batch_size

        for step in range(num_batches):

            eval_tensors = [self.preds, self.loss, self.accuracy, self.precision, self.recall, self.precAtTopK, self.recAtTopK]
            if (step + 1) * self.batch_size % self.log_every == 0:
                print("Evaluating {}, done: {}/{}".format(dataset_type, (step + 1) * self.batch_size, num_batches * self.batch_size))
                eval_tensors += [self.merged_summary_op]
            batch_x, batch_y = self.dataset.getBatch(dataset_type)
            feed_dict = {self.X: batch_x, self.Yoh: batch_y, self.is_training: False}

            eval_ret = self.sess.run(eval_tensors, feed_dict=feed_dict)

            eval_ret = dict(zip(eval_tensors, eval_ret))
            if self.merged_summary_op in eval_tensors:
                if dataset_type == 'valid_train':
                    self.summary_valid_train_writer.add_summary(eval_ret[self.merged_summary_op], self.global_step.eval(session=self.sess))
                else:
                    self.summary_valid_valid_writer.add_summary(eval_ret[self.merged_summary_op], self.global_step.eval(session=self.sess))

            losses.append(eval_ret[self.loss])
            if eval_preds.size == 0:
                labels = np.argmax(batch_y, axis=1)
                eval_preds = np.argmax(eval_ret[self.preds], axis=1)
            else:
                labels = np.concatenate((labels, np.argmax(batch_y, axis=1)), axis=0)
                eval_preds = np.concatenate((eval_preds, np.argmax(eval_ret[self.preds], axis=1)), axis=0)

        total_loss = np.mean(losses)
        acc, pr, rec = util.acc_prec_rec_score(labels, eval_preds)
        print("Validation results -> {} error: epoch {} loss={} accuracy={} precision={} recall={}".format(dataset_type, epoch, total_loss, acc, pr, rec))

        return total_loss, acc, pr, rec

    def startTesting(self):

        print('STARTED TESTING EVALUATION!')

        losses = []
        eval_preds = np.ndarray((0,), dtype=np.int64)
        labels = np.ndarray((0,), dtype=np.int64)

        for step in range(self.dataset.num_batches_test):

            eval_tensors = [self.preds, self.loss, self.accuracy, self.precision, self.recall, self.precAtTopK, self.recAtTopK]
            if (step + 1) * self.batch_size % self.log_every == 0:
                print("Evaluating {}, done: {}/{}".format('test', (step + 1) * self.batch_size, self.dataset.num_test_examples))
                eval_tensors += [self.merged_summary_op]
            batch_x, batch_y = self.dataset.getBatch('test')
            feed_dict = {self.X: batch_x, self.Yoh: batch_y, self.is_training: False}

            eval_ret = self.sess.run(eval_tensors, feed_dict=feed_dict)

            eval_ret = dict(zip(eval_tensors, eval_ret))
            if self.merged_summary_op in eval_tensors:
                self.summary_test_writer.add_summary(eval_ret[self.merged_summary_op], self.global_step.eval(session=self.sess))

            losses.append(eval_ret[self.loss])
            if eval_preds.size == 0:
                labels = np.argmax(batch_y, axis=1)
                eval_preds = eval_ret[self.preds]
            else:
                labels = np.concatenate((labels, np.argmax(batch_y, axis=1)), axis=0)
                eval_preds = np.concatenate((eval_preds,eval_ret[self.preds]), axis=0)

        total_loss = np.mean(losses)
        acc, pr, rec = util.acc_prec_rec_score(labels, np.argmax(eval_preds, axis=1))
        prAtTop10, top5CorrectWords, top5IncorrectWords = util.testStatistics(labels, eval_preds)
        util.write_test_results(total_loss, acc, pr, rec, prAtTop10, top5CorrectWords, top5IncorrectWords)
        print("Validation results -> {} error: epoch {} loss={} accuracy={} precision={} recall={} prAtTop10={}".format('test', '1', total_loss, acc, pr, rec, prAtTop10))

    def initConfig(self):
        print("INITIALIZING CONFIGURATION VARIABLES")
        self.learning_rate = config.config['learning_rate']
        self.regularizer_scale = config.config['regularizer_scale']
        self.w = config.config['input_w']
        self.h = config.config['input_h']
        self.c = config.config['input_c']
        self.frames = config.config['frames']
        self.num_classes = config.config['num_classes']
        self.dataset_name = config.config['dataset']
        self.model_name = "ef"
        self.summary_dir = config.config['summary_data']
        self.saved_session_dir = config.config['saved_session_root_dir']
        self.max_epochs = config.config['max_epochs']
        self.batch_size = config.config['batch_size']
        self.log_every = config.config['log_every']
        self.decay_steps = config.config['decay_steps']
        self.decay_rate = config.config['decay_rate']
        self.save_every = config.config['save_every']

    def createSummaries(self, labelsOH, predsOH, loss, learning_rate):
        labels = layers.onehot_to_class(labelsOH)
        preds = layers.onehot_to_class(predsOH)
        acc = layers.accuracy(labels, preds)
        prec = layers.precision(labels, preds)
        rec = layers.recall(labels, preds)
        precAtTopK = layers.precisionAtTopK(labelsOH, predsOH, 10)
        recAtTopK = layers.recallAtTopK(labelsOH, predsOH, 10)
        tf.summary.scalar('learning_rate', learning_rate)
        tf.summary.scalar('loss', loss)
        tf.summary.scalar('accuracy', acc[1])
        tf.summary.scalar('precision', prec[1])
        tf.summary.scalar('recall', rec[1])
        tf.summary.scalar('precision_top_10', precAtTopK[1])
        tf.summary.scalar('recall_top_10', recAtTopK[1])

        return acc, prec, rec, precAtTopK, recAtTopK

    def initSummaries(self):
        self.merged_summary_op = tf.summary.merge_all()
        self.summary_train_train_writer = tf.summary.FileWriter(self.summary_dir['train_train'], self.sess.graph)
        self.summary_valid_train_writer = tf.summary.FileWriter(self.summary_dir['valid_train'], self.sess.graph)
        self.summary_valid_valid_writer = tf.summary.FileWriter(self.summary_dir['valid_valid'], self.sess.graph)
        self.summary_test_writer = tf.summary.FileWriter(self.summary_dir['test'], self.sess.graph)

    def createSession(self):
        print('CREATING SESSION FOR: {} AND MODEL: {}'.format(self.dataset_name, self.model_name))
        self.saver = tf.train.Saver()
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.9, allow_growth=True)
        gpu_config = tf.ConfigProto(gpu_options=gpu_options, allow_soft_placement=True)
        self.sess = tf.Session(config=gpu_config)
        self.sess.as_default()

    def initSessionVariables(self):
        print("INITIALIZING SESSION VARIABLES")
        init_op = tf.group(tf.global_variables_initializer(), tf.local_variables_initializer())
        self.sess.run(init_op)
        self.coord = tf.train.Coordinator()
        self.threads = tf.train.start_queue_runners(sess=self.sess, coord=self.coord)

    def initDataReaders(self):
        with tf.device("/CPU:0"):
            self.dataset = Dataset(self.sess)

    def finishTraining(self):
        self.coord.request_stop()
        self.coord.join(self.threads)
        self.sess.close()

    def createPlotDataVars(self):
        self.plot_data = {
            'train_loss': [], 'train_acc': [], 'train_pr': [], 'train_rec': [],
            'valid_loss': [], 'valid_acc': [], 'valid_pr': [], 'valid_rec': [],
            'lr': [], 'epoch_time': []
        }
        print("INITIALIZED PLOT DATA!")

    def createRestorer(self):
        if os.path.exists(os.path.join(self.saved_session_dir, self.model_name + '.ckpt.meta')):
            self.saver.restore(self.sess, os.path.join(self.saved_session_dir, config.config['model_name'] + '.ckpt'))

ef = EF()
ef.train()