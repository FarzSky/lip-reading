from cnn import layers
from cnn.abstract_model import AbstractModel
import tensorflow as tf
from utils import config

class EF(AbstractModel):

    def __init__(self):
        super().__init__()
        self.name = 'ef'

    def build(self, logits, is_training=True):

        logits = self.build_local_model(logits, is_training)
        logits = self.build_conv_block3(logits, is_training)

        return logits

    def build_local_model(self, logits, is_training=True):

        with tf.variable_scope(tf.get_variable_scope(), 'ef'):

            stride = 1
            padding = 'same'

            if self.dataset_name != 'mnist':
                logits = layers.rgb_to_grayscale(logits)
                logits = layers.transpose(logits, [0, 2, 3, 1, 4])
                logits = layers.reshape(logits, [-1, self.w, self.h, self.frames])
                stride = 2
                padding = 'valid'

            logits = layers.conv2d(logits, stride=stride, padding=padding, filters=96, name='conv1', activation_fn=None,
                                   kernel_initializer=self.initializer, kernel_regularizer=self.regularizer,
                                   bias_regularizer=self.regularizer)
            # logits = layers.batchNormalization(logits, is_training=is_training)
            logits = layers.relu(logits)
            logits = layers.max_pool2d(logits, pool_size=[3, 3], stride=stride, padding=padding, name='max_pool1')

            logits = layers.conv2d(logits, stride=stride, padding=padding, filters=256, name='conv2', activation_fn=None,
                                   kernel_initializer=self.initializer, kernel_regularizer=self.regularizer,
                                   bias_regularizer=self.regularizer)
            # logits = layers.batchNormalization(logits, is_training=is_training)
            logits = layers.relu(logits)
            logits = layers.max_pool2d(logits, pool_size=[3, 3], stride=stride, padding='same', name='max_pool2')

        return logits