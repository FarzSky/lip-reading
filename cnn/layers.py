import tensorflow as tf
import tensorflow.contrib.layers as contrib_layers

from utils import config
# AKTIVACIJSKE FUNKCIJE

# SELU - aktivacijska funkcija
def selu(x):
    alpha = 1.6733
    scale = 1.0507
    return scale * tf.where(x > 0.0, x, alpha * tf.exp(x) - alpha, 'selu')

# RELU - aktivacijska funkcija
def relu(x):
    return tf.nn.relu(x)

# VJEROJATNOSTNI PROSTOR

# funkcija sigmoide
def sigmoid(x):
    return tf.nn.sigmoid(x)

# softmax
def softmax(x):
    return tf.nn.softmax(x)

# GUBITAK

# gubitak unakrsne entropije sa softmaxom
def softmax_cross_entropy(labels, logits):
    return tf.nn.softmax_cross_entropy_with_logits_v2(logits=logits, labels=labels)

# KONVOLUCIJA

# 2d konvolucija
def conv2d(input, filters=16, kernel_size=[3, 3], padding="same", stride=(1, 1), activation_fn=relu, reuse=None, name="conv2d",
           kernel_initializer=None, bias_initializer=tf.zeros_initializer(), kernel_regularizer=None, bias_regularizer=None):
   return tf.layers.conv2d(input, filters=filters, kernel_size=kernel_size, padding=padding, strides=stride, name=name,
                           activation=activation_fn, reuse=reuse, kernel_initializer=kernel_initializer, bias_initializer=bias_initializer,
                           kernel_regularizer=kernel_regularizer, bias_regularizer=bias_regularizer)

def conv3d(input, filters=16, kernel_size=[3, 3, 3], padding="same", stride=(1, 1, 1), activation_fn=relu, reuse=None, name="conv3d",
           kernel_initializer=None, bias_initializer=tf.zeros_initializer(), kernel_regularizer=None, bias_regularizer=None):
    return tf.layers.conv3d(input, filters=filters, kernel_size=kernel_size, padding=padding, strides=stride, name=name, activation=activation_fn, reuse=reuse,
                            kernel_initializer=kernel_initializer, bias_initializer=bias_initializer,
                            kernel_regularizer=kernel_regularizer, bias_regularizer=bias_regularizer)

# sažimanje maksimalnom vrijednoscu
def max_pool2d(input, pool_size=None, stride=(2, 2), padding='valid', name="max_pool2d"):
    if pool_size is None:
        pool_size = [input.shape[1], input.shape[2]]
    return tf.layers.max_pooling2d(input, pool_size=pool_size, strides=stride, padding=padding, name=name)

def max_pool3d(input, pool_size=None, stride=(2, 2, 2), name='max_pool3d'):
    if pool_size is None:
        pool_size = [input.shape[1], input.shape[2]]
    return tf.layers.max_pooling3d(input, pool_size=pool_size, strides=stride, name=name)

# sazimanje srednjoj vrijednoscu
def avg_pool2d(input, pool_size=None, stride=(1, 1), padding="valid", name="global_pool2d"):
    if pool_size is None:
        pool_size = [input.shape[1], input.shape[2]]
    return tf.layers.average_pooling2d(input, pool_size=pool_size, strides=stride, padding=padding, name=name)

# nova implementacija flattena
def flatten(input, name='flatten'):
    return tf.layers.flatten(input, name=name)

# potpuna povezanost
def fc(input, output_shape, activation_fn, reuse=None, name="fc",
       kernel_initializer=None, bias_initializer=tf.zeros_initializer(), kernel_regularizer=None, bias_regularizer=None):
    return tf.layers.dense(input, output_shape, activation=activation_fn, name=name, reuse=reuse,
                           kernel_initializer=kernel_initializer, bias_initializer=bias_initializer,
                           kernel_regularizer=kernel_regularizer, bias_regularizer=bias_regularizer)

# normalizacija grupe
def batchNormalization(input, is_training=True, reuse=None, axis=-1, name='bn'):
    return tf.layers.batch_normalization(inputs=input, axis=axis, training=is_training, reuse=reuse, name=name)

# add tensors
def add(input1, input2):
    return tf.add(input1, input2)

def lrn (input):
    return tf.nn.local_response_normalization(input)

# SQUEEZE AND EXCITE
def squeeze_and_excite(input, filters=16, scope='se', name='se'):

    filters2 = (int) (filters / config.config['se_r'])
    filters1 = filters

    se = avg_pool2d(input, name='se_avg_pool')
    se = fc(se, filters1, activation_fn=relu, name='se_fc_' + name + '_1')
    se = fc(se, filters2, activation_fn=sigmoid, name='se_fc_' + name + '_2')

    return se

def concat(input, axis=0):
    return tf.concat(input, axis=axis)

def transpose(net, shape):
    return tf.transpose(net, shape)

def reshape(net, shape):
    return tf.reshape(net, shape)

def rgb_to_grayscale(images):
    return tf.image.rgb_to_grayscale(images)

# optimizatori
def adam(learningRate):
    return tf.train.AdamOptimizer(learningRate)

def sgd(learningRate):
    return tf.train.GradientDescentOptimizer(learningRate)

def adagrad(learningRate):
    return tf.train.AdagradOptimizer(learningRate)

def adadelta(learningRate):
    return tf.train.AdadeltaOptimizer(learningRate)

def adagradDA(learningRate):
    return tf.train.AdagradDAOptimizer(learningRate)

def onehot_to_class(Yoh):
    return tf.argmax(Yoh, 1)

def stack(itemList, axis=0):
    return tf.stack(itemList, axis=axis)

def xavier_initializer():
    return contrib_layers.xavier_initializer()

def l1_regularizer(scale=1.0):
    return contrib_layers.l1_regularizer(scale=scale)

def l2_regularizer(scale=1.0):
    return contrib_layers.l2_regularizer(scale=scale)

def accuracy(labels, preds):
    return tf.metrics.accuracy(labels, preds)

def meanPerClassAccuracy(labels, preds, num_classes):
    return tf.metrics.mean_per_class_accuracy(labels, preds, num_classes)

def precision(labels, preds):
    return tf.metrics.precision(labels, preds)

def recall(labels, preds):
    return tf.metrics.recall(labels, preds)

def precisionAtTopK(labels, preds, k):
    return tf.metrics.precision_at_top_k(labels, preds, k)

def recallAtTopK(labels, preds, k):
    return tf.metrics.recall_at_top_k(labels, preds, k)

def decayLearningRate(learning_rate, global_step, decay_steps, decay_rate):
    return tf.train.exponential_decay(learning_rate, global_step, decay_steps, decay_rate, staircase=False, name='learning_rate_decay')
